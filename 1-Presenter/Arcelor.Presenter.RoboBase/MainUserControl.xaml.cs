﻿using System.Windows.Controls;

namespace Robo.Presenter.RoboBase
{
    /// <summary>
    /// Interaction logic for MainUserControl.xaml
    /// </summary>
    public partial class MainUserControl : UserControl
    {
        public MainUserControl()
        {
            InitializeComponent();
        }
    }
}
