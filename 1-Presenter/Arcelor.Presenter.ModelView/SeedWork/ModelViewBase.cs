﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Robo.Presenter.ModelView.SeedWork
{
    public class ModelViewBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(String propertyName = "")
        {
            if(PropertyChanged != null)            
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));            
        }

    }
}
