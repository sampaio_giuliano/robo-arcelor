﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Media.Imaging;

namespace Robo.Presenter.ModelView.SeedWork.ValueConvert
{
    public class ImageConvert
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new BitmapImage(new Uri("pack://application:,,,/Images/" + System.Convert.ToString(value)));
        }
    }
}
