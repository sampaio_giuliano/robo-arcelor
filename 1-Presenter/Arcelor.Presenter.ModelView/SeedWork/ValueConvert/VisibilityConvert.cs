﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Globalization;

namespace Robo.Presenter.ModelView.SeedWork.ValueConvert
{
    public class VisibilityConvert : IValueConverter
    {
        public object Convert(object value, Type tagertType, object parameter, CultureInfo culture)
        {
            bool visibility;
            bool.TryParse(System.Convert.ToString(value), out visibility);
            return visibility ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type tagertType, object parameter, CultureInfo culture)
        {

            Visibility visibility = (Visibility)value;
            return (visibility == Visibility.Visible);
        }
    }
}
