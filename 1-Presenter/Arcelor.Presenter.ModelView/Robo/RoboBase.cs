﻿using Robo.Infrastructure.CrossCutting.SeedWork.Threads;
using Robo.Presenter.ModelView.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Robo.Presenter.ModelView.Robo
{
    public class RoboBase : ModelViewBase
    {
        protected class AcaoRobo
        {
            public int SequenciaExecucao { get; set; }
            public string DescricaoServico { get; set; }
            public Action ExecutarAcao { get; set; }
        }

        protected enum Aba
        {
            MonitorLog = 0,
            Configuracoes = 1
        }

        protected delegate void ThreadParaExecucao(ref bool obj);

        public RoboBase(string tituloForm, bool domingo, bool segunda, bool terca, bool quarta, bool quinta, bool sexta, bool sabado, TimeSpan inicioProcessamento, TimeSpan fimProcessamento, TimeSpan intervaloProcessamento, bool iniciarImediatamenteClickIniciar, int quantidadePorExecucao = 0)
        {            
            _tituloForm = tituloForm;
            _domingo = domingo;
            _segunda = segunda;
            _terca = terca;
            _quarta = quarta;
            _quinta = quinta;
            _sexta = sexta;
            _sabado = sabado;
            _todosDias = _domingo && _segunda && _terca && _quarta && _quinta && _sexta && _sabado;
            _inicioProcessamento = inicioProcessamento;
            _fimProcessamento = fimProcessamento;
            _intervaloProcessamento = intervaloProcessamento;
            _quantidadeExecucao = quantidadePorExecucao;
            _iniciarImediatamenteClickIniciar = iniciarImediatamenteClickIniciar;
            _servicoParado = true;
            this._acoesRobo = new List<AcaoRobo>();
            ConfiguraServico();
        }

        private Aba? _abaSelecionada { get; set; }
        private ThreadService servicoThread { get; set; }
        private bool _servicoParado { get; set; }
        private ICommand _cmdIniciarServico { get; set; }
        private ICommand _cmdPararServico { get; set; }
        private Action _executarServico { get; set; }
        private string _resultadoExecucao { get; set; }
        private string _tituloForm { get; set; }
        private int _quantidadeProcessada { get; set; }
        private int _quantidadeExecucao { get; set; }
        private bool _limiteProcessamento { get { return _quantidadeExecucao == 0 || _quantidadeProcessada < _quantidadeExecucao; } }
        private bool _processarItem
        {
            get
            {
                return servicoThread.EstaEmHorarioDeExecucao && !_servicoParado && _limiteProcessamento;
            }
        }
        private List<AcaoRobo> _acoesRobo { get; set; }
        
        
        private bool _iniciarImediatamenteClickIniciar { get; set; }
        private bool _todosDias { get; set; }
        private bool _segunda { get; set; }
        private bool _terca { get; set; }
        private bool _quarta { get; set; }
        private bool _quinta { get; set; }
        private bool _sexta { get; set; }
        private bool _sabado { get; set; }
        private bool _domingo { get; set; }

        private TimeSpan _inicioProcessamento { get; set; }
        private TimeSpan _fimProcessamento { get; set; }
        private TimeSpan _intervaloProcessamento { get; set; }
        
        public int AbaSelecionada
        {
            get
            {
                _abaSelecionada = _abaSelecionada ?? (_abaSelecionada = RoboBase.Aba.MonitorLog);
                return (int)_abaSelecionada;
            }
            set
            {
                _abaSelecionada = (Aba)value;
            }
        }

        public ICommand cmdIniciarServico
        {
            get
            {
                return _cmdIniciarServico ?? (_cmdIniciarServico = new CommandHandler(IniciarServico, true));
            }
        }

        public bool ServicoParado
        {
            get { return _servicoParado; }
            set
            {
                _servicoParado = value;
                NotifyPropertyChanged();
            }
        }

        public ICommand cmdAPararServico
        {
            get
            {
                return _cmdPararServico ?? (_cmdPararServico = new CommandHandler(new Action(PararServico), true));
            }
        }

        public bool ExecutandoServico
        {
            get { return !ServicoParado; }
            set { ServicoParado = !value; }
        }

        public string ResultadoExecucao
        {
            get { return _resultadoExecucao; }
            set
            {
                _resultadoExecucao = value + Environment.NewLine + _resultadoExecucao;
                NotifyPropertyChanged();
            }
        }

        public string UltimaExecucao
        {
            get { return string.Format("Última execução: {0}", servicoThread.UltimaExecucao); }
        }

        public string ProximaExecucao
        {
            get
            {
                string retorno = string.Empty;

                if(!_servicoParado)
                {
                    retorno = "Aguardando término do serviço para recalcular!";
                }
                else if(_servicoParado && !servicoThread.ProximaExecucao.HasValue)
                {
                    retorno = "Processamento parado. Para retornar ao processamento, clique em iniciar!";
                }
                else
                {
                    retorno = string.Format("{0:dd/MM/yyyy HH:mm:ss}", servicoThread.ProximaExecucao);
                }

                return string.Format("Próxima execução: {0}", retorno);
            }
        }

        public string TituloForm
        {
            get { return string.Format("{0} - Processamento Automático", _tituloForm); }
            set { _tituloForm = value; }
        }

        public bool IniciarImediatamenteClickIniciar
        {
            get { return _iniciarImediatamenteClickIniciar; }
            set
            {
                _iniciarImediatamenteClickIniciar = value;
                servicoThread.IniciarProcessoImediatamente(_iniciarImediatamenteClickIniciar, true);
                NotifyPropertyChanged();
            }
        }

        public bool TodosDias
        {
            get { return _todosDias; }
            set { HabilitarDesabilitarTodosDias(value); }
        }

        public bool Domingo
        {
            get { return _domingo; }
            set
            {
                _domingo = value;
                servicoThread.NoDomingo(_domingo, true);
                NotifyPropertyChanged();
            }
        }

        public bool DomingoHabilitado
        {
            get { return !_todosDias; }
        }

        public bool Segunda
        {
            get { return _segunda; }
            set
            {
                _segunda = value;
                servicoThread.NaSegunda(_segunda, true);
                NotifyPropertyChanged();
            }
        }

        public bool SegundaHabilitado
        {
            get { return !_todosDias; }
        }

        public bool Terca
        {
            get { return _terca; }
            set
            {
                _terca = value;
                servicoThread.NaTerca(_terca, true);
                NotifyPropertyChanged();
            }
        }

        public bool TercaHabilitado
        {
            get { return !_todosDias; }
        }

        public bool Quarta
        {
            get { return _quarta; }
            set
            {
                _quarta = value;
                servicoThread.NaQuarta(_quarta, true);
                NotifyPropertyChanged();
            }
        }

        public bool QuartaHabilitado
        {
            get { return !_todosDias; }
        }

        public bool Quinta
        {
            get { return _quinta; }
            set
            {
                _quinta = value;
                servicoThread.NaQuinta(_quinta, true);
                NotifyPropertyChanged();
            }
        }

        public bool QuintaHabilitado
        {
            get { return !_todosDias; }
        }

        public bool Sexta
        {
            get { return _sexta; }
            set
            {
                _sexta = value;
                servicoThread.NaSexta(_sexta, true);
                NotifyPropertyChanged();
            }
        }

        public bool SextaHabilitado
        {
            get { return !_todosDias; }
        }

        public bool Sabado
        {
            get { return _sabado; }
            set
            {
                _sabado = value;
                servicoThread.NoSabado(_sabado, true);
                NotifyPropertyChanged();
            }
        }

        public bool SabadoHabilitado
        {
            get { return !_todosDias; }
        }

        public DateTime InicioProcessamento
        {
            get { return DateTime.Now.Date.Add(_inicioProcessamento); }
            set
            {
                _inicioProcessamento = value.TimeOfDay;
                servicoThread.InicioProcessamento(_inicioProcessamento, true);
                NotifyPropertyChanged();
            }
        }

        public DateTime FimProcessamento
        {
            get { return DateTime.Now.Date.Add(_fimProcessamento); }
            set
            {
                _fimProcessamento = value.TimeOfDay;
                servicoThread.FimProcessamento(_fimProcessamento, true);
                NotifyPropertyChanged();
            }
        }

        public DateTime IntervaloProcessamento
        {
            get { return DateTime.Now.Date.Add(_intervaloProcessamento); }
            set
            {
                _intervaloProcessamento = value.TimeOfDay;
                servicoThread.IntervaloProcessamento(_intervaloProcessamento, true);
                NotifyPropertyChanged();
            }
        }

        public int QuantidadePorExecucao
        {
            get { return _quantidadeExecucao; }
            set
            {
                _quantidadeExecucao = value;
                NotifyPropertyChanged();
            }
         }

        protected RoboBase AdicionarAcaoRobo(Action acao, int sequencia, string descricao)
        {
            this._acoesRobo.Add(new AcaoRobo { SequenciaExecucao = sequencia, DescricaoServico = descricao, ExecutarAcao = acao });
            return this;
        }

        protected void IniciarServico()
        {
            servicoThread.Iniciar();
            NotifyPropertyChanged();
        }

        protected void AguardarExecucaoThread(ThreadParaExecucao threadExecucao, Action finalizaThread = null)
        {
            bool executando = false;

            try
            {
                if(threadExecucao == null)
                {
                    throw new Exception("Ação não definida para execução");
                }
                executando = false;
                threadExecucao.Invoke(ref executando);
            }
            catch(Exception ex)
            {
                ResultadoExecucao = ex.Message;
                executando = false;
            }
            finally
            {
                while(executando)
                {
                    continue;
                }

                if(finalizaThread != null)
                {
                    finalizaThread.Invoke();
                }
            }
        }

        protected void RegistrarResultado(string resultado)
        {
            ResultadoExecucao = resultado;
        }

        private void ConfiguraServico()
        {
            servicoThread = new ThreadService();

            servicoThread.OnExecutarProcesso(ExecutarServicoRobo)
                .OnProcessoFinalizado(() => { NotifyPropertyChanged(); })
                .InicioProcessamento(_inicioProcessamento, false)
                .IntervaloProcessamento(_intervaloProcessamento, false)
                .FimProcessamento(_fimProcessamento, false)
                .NoDomingo(_domingo, false)
                .NaSegunda(_segunda, false)
                .NaTerca(_terca, false)
                .NaQuarta(_quarta, false)
                .NaQuinta(_quinta, false)
                .NaSexta(_sexta, false)
                .NoSabado(_sabado, false)
                .TodosOsDias(_todosDias, false)
                .IniciarProcessoImediatamente(_iniciarImediatamenteClickIniciar, false);
        }

        private void ExecutarServicoRobo()
        {
            try
            {
                if(this._servicoParado)
                {
                    this._servicoParado = false;
                    this._resultadoExecucao = string.Empty;

                    ResultadoExecucao = string.Format("Iniciando a execução do serviço as {0:dd/MM/yyyy HH:mm:ss}",DateTime.Now);
                    NotifyPropertyChanged();

                    if(_acoesRobo == null || _acoesRobo.Count == 0)
                    {
                        throw new Exception("Nenhuma ação definida para esse robô");
                    }


                    foreach(var acao in _acoesRobo.OrderBy(x=> x.SequenciaExecucao))
                    {
                        try
                        {
                            ResultadoExecucao = acao.DescricaoServico;
                            ConfirmarHorarioServico();
                            acao.ExecutarAcao.Invoke();
                            NotifyPropertyChanged();
                        }
                        catch(Exception ex)
                        {
                            ResultadoExecucao = ex.Message;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                ResultadoExecucao = ex.Message;
            }
            finally
            {
                NotificarFinalizacaoServico();
            }
        }

        private void PararServico()
        {
            this._servicoParado = true;
            servicoThread.Parar();
            NotifyPropertyChanged();
        }

        private void ConfirmarHorarioServico()
        {
            if (!servicoThread.EstaEmHorarioDeExecucao)
                throw new Exception("Processo finalizado devido ao horário de operação do robô");
        }

        private void NotificarFinalizacaoServico()
        {
            _servicoParado = true;
            ResultadoExecucao = string.Format("Serviço finalizado as {0:dd/MM/yyyy HH:mm:ss}", DateTime.Now);
        }

        private void HabilitarDesabilitarTodosDias(bool todosDiasSelecionado)
        {
            _todosDias = todosDiasSelecionado;

            if(_todosDias)
            {
                _domingo = _segunda = _terca = _quarta = _quinta = _segunda = _sabado = _todosDias;
            }

            servicoThread.NoDomingo(_domingo, false).NaSegunda(_segunda, false).NaTerca(_terca, false).NaQuarta(_quarta, false).NaQuinta(_quinta, false).NaSexta(_segunda, false).NoSabado(_sabado, false).TodosOsDias(_todosDias, true);
            NotifyPropertyChanged();
        }
    }
}
