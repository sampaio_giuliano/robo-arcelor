﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Robo.Presenter.ModelView.Robo
{
    public class RoboPrincipal : RoboBase
    {
        public RoboPrincipal() : base("Robo 1.5", true, true, true, true, true, true, true, new TimeSpan(00, 00, 0), new TimeSpan(23, 59, 0), new TimeSpan(0, 15, 0), true, 0)
        {            
            base.AdicionarAcaoRobo(new Action(ProcessarServico), 0, "Executando Robo 1.5");
        }

        private void ProcessarServico()
        {
            ThreadParaExecucao processamento = new ThreadParaExecucao((ref bool executando) =>
            {

                using (var ServiceRobo = new RoboApp.Application.Service.Servico.RoboArcelorService())
                {
                    executando = true;
                    ServiceRobo.OnEnvioDeInformacao = RegistrarResultado;
                    ServiceRobo.ProcessarServico(base.InicioProcessamento.TimeOfDay, base.FimProcessamento.TimeOfDay, ref executando, base.QuantidadePorExecucao, ConfigurationManager.AppSettings["TextoSMS"], base.IntervaloProcessamento.TimeOfDay);
                }
            });

            AguardarExecucaoThread(processamento);
        }
    }
}
