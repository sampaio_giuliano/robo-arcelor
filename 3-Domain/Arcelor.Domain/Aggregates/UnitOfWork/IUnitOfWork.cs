﻿using Robo.Domain.Aggregates.RepositoryContract;
using System;

namespace Robo.Domain.Aggregates.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IControleMidasRepository ControleMidasRepository { get; }
        ITRPAACSMID0Repository TRPAACSMID0Repository { get; }
        ITRPACTLMID2Repository TRPACTLMID2Repository { get; }
        IAuditoriaRepository AuditoriaRepository { get; }
        void Commit();
    }
}
