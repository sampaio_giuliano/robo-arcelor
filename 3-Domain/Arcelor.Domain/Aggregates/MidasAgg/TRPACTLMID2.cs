﻿
using System;

namespace Robo.Domain.Aggregates.ClienteAgg
{
    public class TRPACTLMID2
    {
        public Int64 ID_REG { get; set; }
        public string CAP_ID_NOT { get; set; }
        public string CAP_NUM_CHM { get; set; }
        public string MID_ID_EVO { get; set; }
        public string CAP_NOM_ANX { get; set; }
        public string DAT_CRI { get; set; }
        public string DAT_ATU { get; set; }
        public string XML_ANX { get; set; }
        public string XML_ANX_ORI { get; set; }
        public string TIP_XML { get; set; }
        public string MID_ID_ITM_ANX { get; set; }
        public string MID_NOM_ITM_ANX { get; set; }
        public string XML_VER { get; set; }
        public string XML_DAT_CMP { get; set; }
        public string STU_DOC_FIS { get; set; }
        public string STU_CEPOM { get; set; }
        public string STU_PREF { get; set; }
    }
}
