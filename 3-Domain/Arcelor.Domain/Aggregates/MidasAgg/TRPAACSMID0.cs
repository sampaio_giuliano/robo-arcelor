﻿
using System;

namespace Robo.Domain.Aggregates.ClienteAgg
{
    public class TRPAACSMID0
    {
        public Int64 ID_REG { get; set; }
        public string TS_ACS { get; set; }
        public string CD_LGN { get; set; }
        public string NM_SER { get; set; }        
    }
}
