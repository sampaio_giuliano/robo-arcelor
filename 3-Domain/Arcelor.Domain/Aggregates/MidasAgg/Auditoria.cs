﻿
using System;

namespace Robo.Domain.Aggregates.ClienteAgg
{
    public class Auditoria
    {
        public int Id { get; set; }
        public string IdExecucao { get; set; }
        public string TipoAgente { get; set; }
        public string DataHoraIni { get; set; }
        public string DataHoraFim { get; set; }
        public int? DuracaoProcesso { get; set; }
        public string CategoriaProcesso { get; set; }
        public string CodigoProcesso { get; set; }
        public string ItemProcesso { get; set; }
        public string SubitemProcesso { get; set; }
        public string NomeProcesso { get; set; }
        public string StatusProcesso { get; set; }
        public string MotivoProcesso { get; set; }
        public string NomeEquipamento { get; set; }
        public string IpEquipamento { get; set; }
        public string VersaoRobo { get; set; }
    }
}
