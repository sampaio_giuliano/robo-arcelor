﻿
using System;

namespace Robo.Domain.Aggregates.ClienteAgg
{
    public class ControleMidas
    {
        public Int64 ID_REG { get; set; }
        public string ID_EVO { get; set; }
        public string CHM_CAP { get; set; }
        public DateTime TS_EVO { get; set; }        
        public string STU_RB_FIS { get; set; }
        public string STU_XML { get; set; }
        public string STU_PRC { get; set; }
        public Int64 QTD_ANX_CHM { get; set; }
        public string UND_OPE { get; set; }
        public Int64 NUM_INR { get; set; }
        public string TIP_INR { get; set; }
        public decimal VAL_INR { get; set; }
        public string CPF_CNPJ { get; set; }
        public DateTime DAT_CRI { get; set; }
        public string DAT_ATU { get; set; }
        public string LOC_CHM { get; set; }        
    }
}
