﻿using Robo.Domain.Aggregates.ClienteAgg;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Robo.Domain.Aggregates.RepositoryContract
{
    public interface ITRPACTLMID2Repository : IRepositoryBase<TRPACTLMID2>
    {
        int UpdateTRPACTLMID2(TRPACTLMID2 entity);

        int AddTRPACTLMID2(TRPACTLMID2 entity);

        IEnumerable<TRPACTLMID2> GetTRPACTLMID2(TRPACTLMID2 entity);

        IEnumerable<TRPACTLMID2> GetTRPACTLMID2CHAMADOMIDAS(TRPACTLMID2 entity);

        IEnumerable<TRPACTLMID2> GetTRPACTLMID2CLASSIFICACAO(TRPACTLMID2 entity);
    }
}
