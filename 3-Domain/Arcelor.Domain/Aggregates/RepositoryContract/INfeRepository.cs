﻿using Robo.Domain.Aggregates.MidasAgg;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robo.Domain.Aggregates.RepositoryContract
{
    public interface INfeRepository : IRepositoryBase<Nfe>
    {
        Nfe GetLastNFe();

        bool SetXML(string xml);
    }
}
