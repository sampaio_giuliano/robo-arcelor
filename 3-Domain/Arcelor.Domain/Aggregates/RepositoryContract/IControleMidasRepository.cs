﻿using Robo.Domain.Aggregates.ClienteAgg;
using Robo.Domain.Aggregates.MidasAgg;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Robo.Domain.Aggregates.RepositoryContract
{
    public interface IControleMidasRepository : IRepositoryBase<ControleMidas>
    {
        IEnumerable<ControleMidas> GetControleMidas();

        int UpdateTRPACTLMID0(ControleMidas entity);

        int GetCRPAGETXML0(ControleMidas entity);
     
    }
}
