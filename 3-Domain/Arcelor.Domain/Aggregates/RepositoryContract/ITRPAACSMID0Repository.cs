﻿using Robo.Domain.Aggregates.ClienteAgg;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Robo.Domain.Aggregates.RepositoryContract
{
    public interface ITRPAACSMID0Repository : IRepositoryBase<TRPAACSMID0>
    {
        int AddTRPAACSMID0(TRPAACSMID0 entity);
    }
}
