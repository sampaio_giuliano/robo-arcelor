﻿using Robo.Domain.Aggregates.ClienteAgg;

namespace Robo.Domain.Aggregates.RepositoryContract
{
    public interface IAuditoriaRepository : IRepositoryBase<Auditoria>
    {
        int AddAuditoria(Auditoria entity);
    }
}
