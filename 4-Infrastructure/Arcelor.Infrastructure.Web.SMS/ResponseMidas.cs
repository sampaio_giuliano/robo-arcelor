﻿namespace Robo.Infrastructure.Web.Midas
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false)]
    public partial class Envelope
    {
        private EnvelopeHeader headerField;

        private EnvelopeBody bodyField;

        /// <remarks/>
        public EnvelopeHeader Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        public EnvelopeBody Body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public partial class EnvelopeHeader
    {

        private Action actionField;

        private string relatesToField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.w3.org/2005/08/addressing")]
        public Action Action
        {
            get
            {
                return this.actionField;
            }
            set
            {
                this.actionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.w3.org/2005/08/addressing")]
        public string RelatesTo
        {
            get
            {
                return this.relatesToField;
            }
            set
            {
                this.relatesToField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2005/08/addressing")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2005/08/addressing", IsNullable = false)]
    public partial class Action
    {

        private int mustUnderstandField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
        public int mustUnderstand
        {
            get
            {
                return this.mustUnderstandField;
            }
            set
            {
                this.mustUnderstandField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public partial class EnvelopeBody
    {

        private ResponseMidas authenticateRequestResponseField;

        private GetStatusPreOccurrenceResponse getStatusPreOccurrenceResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://tempuri.org/")]
        public ResponseMidas AuthenticateRequestResponse
        {
            get
            {
                return this.authenticateRequestResponseField;
            }
            set
            {
                this.authenticateRequestResponseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://tempuri.org/")]
        public GetStatusPreOccurrenceResponse GetStatusPreOccurrenceResponse
        {
            get
            {
                return this.getStatusPreOccurrenceResponseField;
            }
            set
            {
                this.getStatusPreOccurrenceResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://tempuri.org/", IsNullable = false)]
    public partial class ResponseMidas
    {

        private string authenticateRequestResultField;

        /// <remarks/>
        public string AuthenticateRequestResult
        {
            get
            {
                return this.authenticateRequestResultField;
            }
            set
            {
                this.authenticateRequestResultField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://tempuri.org/", IsNullable = false)]
    public partial class GetStatusPreOccurrenceResponse
    {
        private PreOccurrenceStatusOutput[] getStatusPreOccurrenceResultField;        

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PreOccurrenceStatusOutput", Namespace = "http://schemas.datacontract.org/2004/07/Argo.WCF.Model", IsNullable = false)]
        public PreOccurrenceStatusOutput[] GetStatusPreOccurrenceResult
        {
            get
            {
                return this.getStatusPreOccurrenceResultField;
            }
            set
            {
                this.getStatusPreOccurrenceResultField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
    public partial class GetStatusPreOccurrenceResponseGetStatusPreOccurrenceResult
    {

        private PreOccurrenceStatusOutput preOccurrenceStatusOutputField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.datacontract.org/2004/07/Argo.WCF.Model")]
        public PreOccurrenceStatusOutput PreOccurrenceStatusOutput
        {
            get
            {
                return this.preOccurrenceStatusOutputField;
            }
            set
            {
                this.preOccurrenceStatusOutputField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.datacontract.org/2004/07/Argo.WCF.Model")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.datacontract.org/2004/07/Argo.WCF.Model", IsNullable = false)]
    public partial class PreOccurrenceStatusOutput
    {

        private string channelTypeField;

        private object commentsField;

        private System.DateTime creationDateField;

        private bool hasErrorField;

        private PreOccurrenceStatusOutputItemPreOccurrenceStatusOutput[] itemsField;

        private int itensCountField;

        private uint preOccurrenceIdField;

        private string statusField;

        /// <remarks/>
        public string ChannelType
        {
            get
            {
                return this.channelTypeField;
            }
            set
            {
                this.channelTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public object Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        /// <remarks/>
        public System.DateTime CreationDate
        {
            get
            {
                return this.creationDateField;
            }
            set
            {
                this.creationDateField = value;
            }
        }

        /// <remarks/>
        public bool HasError
        {
            get
            {
                return this.hasErrorField;
            }
            set
            {
                this.hasErrorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ItemPreOccurrenceStatusOutput", IsNullable = false)]
        public PreOccurrenceStatusOutputItemPreOccurrenceStatusOutput[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        public int ItensCount
        {
            get
            {
                return this.itensCountField;
            }
            set
            {
                this.itensCountField = value;
            }
        }

        /// <remarks/>
        public uint PreOccurrenceId
        {
            get
            {
                return this.preOccurrenceIdField;
            }
            set
            {
                this.preOccurrenceIdField = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.datacontract.org/2004/07/Argo.WCF.Model")]
    public partial class PreOccurrenceStatusOutputItemPreOccurrenceStatusOutput
    {

        private object accessKeyField;

        private object commentsField;

        private string fileNameField;

        private int idField;

        private object integrationDocumentField;

        private object masterDataDocumentField;

        private int occurrenceIdField;

        private string statusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public object AccessKey
        {
            get
            {
                return this.accessKeyField;
            }
            set
            {
                this.accessKeyField = value;
            }
        }

        /// <remarks/>
        public object Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        /// <remarks/>
        public string FileName
        {
            get
            {
                return this.fileNameField;
            }
            set
            {
                this.fileNameField = value;
            }
        }

        /// <remarks/>
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public object IntegrationDocument
        {
            get
            {
                return this.integrationDocumentField;
            }
            set
            {
                this.integrationDocumentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public object MasterDataDocument
        {
            get
            {
                return this.masterDataDocumentField;
            }
            set
            {
                this.masterDataDocumentField = value;
            }
        }

        /// <remarks/>
        public int OccurrenceId
        {
            get
            {
                return this.occurrenceIdField;
            }
            set
            {
                this.occurrenceIdField = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }
       
}
