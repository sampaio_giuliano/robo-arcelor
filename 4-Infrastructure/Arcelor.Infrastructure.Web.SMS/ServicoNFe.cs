﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using RestSharp;
using Robo.Infrastructure.RoboBase;
//using Robo.Infrastructure.Web.Midas.Responses.TestNFe;
using Robo.Infrastructure.Web.Midas.Responses.NFe;

namespace Robo.Infrastructure.Web.Midas
{
    public class ServicoNFe 
    {
        protected string authUrlRequest = "";

        public Responses.NFe.Envelope AuthenticateRequest(string login, string password)
        {
            try
            {
                var client = new RestClient(this.authUrlRequest);
                var request = new RestRequest(Method.POST);

                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", "application/soap+xml");


                return Deserialize<Responses.NFe.Envelope>(client.Execute(request).Content);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string[] GetNfes(string key)
        {
            var uri = ConfigurationManager.AppSettings["UriWs"];
            var client = new RestClient(uri);

            var login = ConfigurationManager.AppSettings["UsuarioWs"];
            var senha = ConfigurationManager.AppSettings["SenhaWs"];

            client.Authenticator = new RestSharp.Authenticators.HttpBasicAuthenticator(login, senha);
             
            var request = new RestRequest(Method.POST);
            var body = this.GetRequestXml(key);

            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("charset", "utf-8");
            request.AddHeader("content-type", "text/xml");
            request.AddHeader("SOAPAction", "http://sap.com/xi/WebService/soap1.1");

            request.AddParameter("text/xml", body, ParameterType.RequestBody);
           
            var content  = client.Execute(request).Content;
            var response = Deserialize<Responses.NFe.Envelope>(content);

            return response.Body.MT_Retorno_Pesquisa_Robo.ET_XML;
        }

        public string GetRequestXml(string key)
        {
            return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:BuscaNFeRobo:GRC:BuscaNFeSaidaRobo\"><soapenv:Header/><soapenv:Body><urn:MT_Envio_Pesquisa><chave>"+key+"</chave></urn:MT_Envio_Pesquisa></soapenv:Body></soapenv:Envelope>";
            
            //return "<?xml version=\"1.0\" encoding=\"utf-8\"?><soapenv:Envelope " +
            //    "xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
            //    "xmlns:urn=\"urn:BuscaNFeRobo:GRC:BuscaNFeSaidaRobo\">" +
            //       "<soapenv:Header/>" +
            //       "<soapenv:Body>" +
            //       "<urn:MT_Envio_Pesquisa>" +
            //       "<chave>" + key + "</chave>" +
            //       "</urn:MT_Envio_Pesquisa>" +
            //       "</soapenv:Body>" +
            //       "</soapenv:Envelope>";

            // return "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><BuscaNFeSaidaRobo xmlns=\"http://tempuri.org/\"/></soap:Body></soap:Envelope>";
        }


        public T Deserialize<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        public string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
