﻿using RestSharp;
using Robo.Infrastructure.RoboBase;
using System;
using System.IO;
using System.Xml.Serialization;

namespace Robo.Infrastructure.Web.Midas
{
    public class ServicoMidas : BaseRobo, IDisposable
    {
        private bool disposed = false;
        private string authUrlRequest = "https://nix.midassolutions.com.br/prd/035/wcf/Argo.WCF.WCFNix.svc/AuthenticateRequest";

        public Envelope AuthenticateRequest(string login, string password)
        {
            try
            {                
                var client = new RestClient(this.authUrlRequest);                
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", "application/soap+xml");
                request.AddParameter("undefined", "<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:a=\"http://www.w3.org/2005/08/addressing\"><s:Header><a:Action s:mustUnderstand=\"1\">http://tempuri.org/Midas_x0020_Solutions_x0020_Soap/AuthenticateRequest</a:Action><a:MessageID>urn:uuid:f2bbc68a-76c4-409b-a261-58016d8de1a5</a:MessageID><a:ReplyTo><a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address></a:ReplyTo><a:To s:mustUnderstand=\"1\">https://nix.midassolutions.com.br/prd/035/wcf/Argo.WCF.WCFNix.svc</a:To></s:Header><s:Body><AuthenticateRequest xmlns=\"http://tempuri.org/\"><login>" + login + "</login><password>" + password + "</password></AuthenticateRequest></s:Body></s:Envelope>", ParameterType.RequestBody);

                return Deserialize<Envelope>(client.Execute(request).Content);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Envelope GetStatusPreOccurrence(string sessionId, string preOccurrenceId)
        {
            try
            {
                string preOccurrenceIds = string.Empty;

                foreach (var item in preOccurrenceId.Split(','))
                {
                    if(!string.IsNullOrEmpty(item))
                        preOccurrenceIds += "<c:int>" + item + "</c:int>";
                }

                var client = new RestClient("https://nix.midassolutions.com.br/prd/035/wcf/Argo.WCF.WCFNix.svc");
                var request = new RestRequest(Method.POST);

                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("charset", "utf-8");
                request.AddHeader("content-type", "application/soap+xml");
                request.AddParameter("application/soap+xml", "<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:a=\"http://www.w3.org/2005/08/addressing\"><s:Header><a:Action s:mustUnderstand=\"1\">http://tempuri.org/Midas_x0020_Solutions_x0020_Soap/GetStatusPreOccurrence</a:Action><a:MessageID>urn:uuid:c731504c-174a-43a7-a349-8cdf26d5210d</a:MessageID><a:ReplyTo><a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address></a:ReplyTo><a:To s:mustUnderstand=\"1\">https://nix.midassolutions.com.br/prd/035/wcf/Argo.WCF.WCFNix.svc</a:To></s:Header><s:Body><GetStatusPreOccurrence xmlns=\"http://tempuri.org/\"><preOccurrenceStatusInput xmlns:b=\"http://schemas.datacontract.org/2004/07/Argo.WCF.Model\" xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"><b:PreOccurrenceId xmlns:c=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\">" + preOccurrenceIds + "</b:PreOccurrenceId><b:SessionId>" + sessionId + "</b:SessionId></preOccurrenceStatusInput></GetStatusPreOccurrence></s:Body></s:Envelope>", ParameterType.RequestBody);

                string resultado = client.Execute(request).Content;

                return Deserialize<Envelope>(resultado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public T Deserialize<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        public string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }

        public void Dispose()
        {            
            GC.SuppressFinalize(this);
        }
    }
}
