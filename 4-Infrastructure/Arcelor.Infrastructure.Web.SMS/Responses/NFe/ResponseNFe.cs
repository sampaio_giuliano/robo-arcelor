﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robo.Infrastructure.Web.Midas.Responses.NFe
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.xmlsoap.org/soap/envelope/", IsNullable = false)]
    public partial class Envelope
    {

        private EnvelopeBody bodyField;

        /// <remarks/>
        public EnvelopeBody Body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public partial class EnvelopeBody
    {

        private MT_Retorno_Pesquisa_Robo mT_Retorno_Pesquisa_RoboField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "urn:BuscaNFeRobo:GRC:BuscaNFeSaidaRobo")]
        public MT_Retorno_Pesquisa_Robo MT_Retorno_Pesquisa_Robo
        {
            get
            {
                return this.mT_Retorno_Pesquisa_RoboField;
            }
            set
            {
                this.mT_Retorno_Pesquisa_RoboField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "urn:BuscaNFeRobo:GRC:BuscaNFeSaidaRobo")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "urn:BuscaNFeRobo:GRC:BuscaNFeSaidaRobo", IsNullable = false)]
    public partial class MT_Retorno_Pesquisa_Robo
    {

        private string[] eT_XMLField;

        private string eV_CHAVEField;

        private byte eV_LIMITE_REGField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Namespace = "")]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", IsNullable = false)]
        public string[] ET_XML
        {
            get
            {
                return this.eT_XMLField;
            }
            set
            {
                this.eT_XMLField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "", DataType = "integer")]
        public string EV_CHAVE
        {
            get
            {
                return this.eV_CHAVEField;
            }
            set
            {
                this.eV_CHAVEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public byte EV_LIMITE_REG
        {
            get
            {
                return this.eV_LIMITE_REGField;
            }
            set
            {
                this.eV_LIMITE_REGField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ET_XML
    {

        private string[] itemField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("item")]
        public string[] item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }


}
