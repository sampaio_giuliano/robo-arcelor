﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Robo.Infrastructure.Web.Kibana
{
    public class Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int skipped { get; set; }
        public int failed { get; set; }
    }

    public class Beat
    {
        public string hostname { get; set; }
        public string index { get; set; }
        public string name { get; set; }
        public string version { get; set; }
    }

    public class Headers
    {
        public string authorization { get; set; }
        public string connection { get; set; }        
        public string host { get; set; }
        public string messageid { get; set; }
        public string userid { get; set; }
        public string version_name { get; set; }
    }

    public class Query
    {
        public string userID { get; set; }
    }

    public class Request
    {
        public string body { get; set; }
        public Headers headers { get; set; }
        public string path { get; set; }
        public string pathFull { get; set; }
        public Query query { get; set; }
        public string verb { get; set; }
    }

    public class Cliente
    {
        public Int64 codigo { get; set; }
        public string cpf { get; set; }
        public string digito { get; set; }
        public string nome { get; set; }
    }

    public class Status
    {
        public int codigo { get; set; }
        public string descricao { get; set; }
    }

    public class Mmp
    {
        public string dataAtualizacao { get; set; }
        public Status status { get; set; }
        public decimal valorParcelamento10x { get; set; }
        public decimal valorParcelamento18x { get; set; }
    }

    public class Status2
    {
        public string codigo { get; set; }
        public string descricao { get; set; }
    }

    public class Proposta
    {
        public Status2 status { get; set; }
    }

    public class Status3
    {
        public int codigo { get; set; }
        public string descricao { get; set; }
    }

    public class OriginacaoAtiva
    {
        public decimal limiteCredito { get; set; }
        public Proposta proposta { get; set; }
        public Status3 status { get; set; }
        public int valorParcela { get; set; }
    }

    public class Filial
    {
        public int codigo { get; set; }
        public int codigoParceiro { get; set; }
    }

    public class Vendedor
    {
        public int codigo { get; set; }
        public int codigoVendedor { get; set; }
        public string cpf { get; set; }
        public Filial filial { get; set; }
        public string nome { get; set; }
    }

    public class Body
    {
        public Cliente cliente { get; set; }
        public string mensagem { get; set; }
        public Mmp mmp { get; set; }
        public OriginacaoAtiva originacaoAtiva { get; set; }
        public Vendedor vendedor { get; set; }
    }    

    public class Response
    {
        public Body body { get; set; }
        public int elapsedTime { get; set; }        
        public int statusCode { get; set; }
    }
    
    public class Logging
    {
        public int elapsedTime { get; set; }
        public Request request { get; set; }
        public Response response { get; set; }        
    }

    public class Message
    {
        public string timestamp { get; set; }
        public Beat beat { get; set; }
        public int elapsedTime { get; set; }
        public string env { get; set; }
        public string hostname { get; set; }
        public string level { get; set; }
        public Logging logging { get; set; }
        public string message { get; set; }
        public string messageId { get; set; }
        public string name { get; set; }
        public int pid { get; set; }
        public string source { get; set; }
        public List<string> tags { get; set; }
        public string type { get; set; }
        public string version { get; set; }
    }

    public class Source
    {
        public Beat beat { get; set; }
        public string message { set { mensagem = JsonConvert.DeserializeObject<Message>(Convert.ToString(JsonConvert.DeserializeObject<dynamic>(value))); } }
        public string type { get; set; }
        public string timestamp { get; set; }        
        public Message mensagem { get; set; }
        public string received_on { get; set; }        
    }

    public class Fields
    {
        public List<long> timestamp { get; set; }
        public List<long> received_on { get; set; }
    }

    public class Hit
    {     
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public int _version { get; set; }
        public string _score { get; set; }
        public Source _source { get; set; }        
        public List<object> sort { get; set; }
        public Fields fields { get; set; }
    }

    public class Hits
    {
        public int total { get; set; }
        public int? max_score { get; set; }
        public List<Hit> hits { get; set; }
    }

    public class Bucket
    {
        public string key_as_string { get; set; }
        public string key { get; set; }
        public int doc_count { get; set; }
    }

    public class Aggregation
    {
        public List<Bucket> buckets { get; set; }
    }

    public class Aggregations
    {
        public Aggregation aggregation { get; set; }
    }

    public class Respons
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public Shards _shards { get; set; }
        public Hits hits { get; set; }
        public Aggregations aggregations { get; set; }
        public int status { get; set; }
    }

    public class RespostaKibana
    {
        public List<Respons> responses { get; set; }
    }
}
