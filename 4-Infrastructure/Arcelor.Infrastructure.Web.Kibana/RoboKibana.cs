﻿
using Robo.Infrastructure.RoboBase;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Robo.Infrastructure.Web.Kibana
{
    public class RoboKibana : BaseRobo, IDisposable
    {
        public RoboKibana() { }

        public RespostaKibana ConsultarDados(string usuario, string senha)
        {
            var Basicbase64 = Convert.ToBase64String(Encoding.Default.GetBytes(string.Format("{0}:{1}", usuario, senha)));
            base.CacheControl("no-cache");
            base.Authorization(string.Format("Basic {0}", Basicbase64));
            base.AcceptLanguage("pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7");
            base.UserAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36");
            base.KbnVersion("5.5.0");
            base.ContentType("application/json");

            var preference = RobotUtils.ConvertToTimestamp(DateTime.Now);
            var gte = RobotUtils.ConvertToTimestamp(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,DateTime.Now.AddMinutes(-10).Hour, DateTime.Now.AddMinutes(-10).Minute, 0,0));
            var lte = RobotUtils.ConvertToTimestamp(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.TimeOfDay.Hours, DateTime.Now.Minute, 0, 0));
            
            RestSharp.Parameter parameter = new RestSharp.Parameter();
            parameter.Name = "undefined";
            parameter.Value = "{\"index\":[\"lojas-cliente-crm-*\"],\"ignore_unavailable\":true,\"preference\":" + preference + "} \\n\r\n{\"version\":true,\"size\":10000,\"sort\":[{\"@timestamp\":{\"order\":\"desc\",\"unmapped_type\":\"boolean\"}}],\"query\":{\"bool\":{\"must\":[{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"*\"}},{\"range\":{\"@timestamp\":{\"gte\":"+ gte + ",\"lte\":"+ lte +",\"format\":\"epoch_millis\"}}}],\"must_not\":[]}},\"_source\":{\"excludes\":[]},\"aggs\":{\"aggregation\":{\"date_histogram\":{\"field\":\"@timestamp\",\"interval\":\"10m\",\"time_zone\":\"America/Sao_Paulo\",\"min_doc_count\":1}}},\"stored_fields\":[\"*\"],\"script_fields\":{},\"docvalue_fields\":[\"@received_on\",\"@timestamp\",\"logging.response.body.compras.channels.last.date\"],\"highlight\":{\"pre_tags\":[\"@kibana-highlighted-field@\"],\"post_tags\":[\"@/kibana-highlighted-field@\"],\"fields\":{\"*\":{\"highlight_query\":{\"bool\":{\"must\":[{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"*\",\"all_fields\":true}},{\"range\":{\"@timestamp\":{\"gte\":"+ gte +",\"lte\":"+ lte +",\"format\":\"epoch_millis\"}}}],\"must_not\":[]}}}},\"fragment_size\":0}}\r\n";
            parameter.Type = RestSharp.ParameterType.RequestBody;

            var resultado = base.PostJson("https://kibana.burzum.appsluiza.com.br/elasticsearch/_msearch", parameter);
            
            return resultado.ToObject<RespostaKibana>();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}    
