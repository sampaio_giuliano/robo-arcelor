﻿using Dapper;
using Robo.Domain.Aggregates.ClienteAgg;
using Robo.Domain.Aggregates.RepositoryContract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Robo.Infrastructure.Data.Repository
{
    internal class TRPACTLMID2Repository : RepositoryBase, ITRPACTLMID2Repository
    {
        public TRPACTLMID2Repository(IDbTransaction transaction)
            : base(transaction)
        {            
        }       

        public IEnumerable<TRPACTLMID2> GetTRPACTLMID2(TRPACTLMID2 entity)
        {
            var query = "SP_RPA_CONSULTAR_TRPACTLMID2";

            var param = new DynamicParameters();
            param.Add("@MID_ID_EVO", entity.MID_ID_EVO);
            param.Add("@CAP_NUM_CHM", entity.CAP_NUM_CHM);
            param.Add("@CAP_NOM_ANX", entity.CAP_NOM_ANX);

            var list = Connection.Query<TRPACTLMID2>(query, param, transaction: Transaction, commandType: CommandType.StoredProcedure);

            return list;
        }

        public IEnumerable<TRPACTLMID2> GetTRPACTLMID2CHAMADOMIDAS(TRPACTLMID2 entity)
        {
            var query = "SELECT ID_REG, MID_ID_EVO, CAP_NOM_ANX FROM dbo.TRPACTLMID2 WHERE MID_ID_EVO='" + entity.MID_ID_EVO + "' AND XML_ANX IS NOT NULL";

            return Connection.Query <TRPACTLMID2>(query, null, transaction: Transaction, commandType: CommandType.Text);
        }

        public IEnumerable<TRPACTLMID2> GetTRPACTLMID2CLASSIFICACAO(TRPACTLMID2 entity)
        {
            var query = "SELECT ISNULL(XML_ANX, '') FROM dbo.TRPACTLMID2 WHERE CAP_NUM_CHM='" + entity.CAP_NUM_CHM + "' AND CAP_NOM_ANX='"+ entity.CAP_NOM_ANX +"' AND MID_ID_EVO='"+ entity.MID_ID_EVO +"'";

            return Connection.Query<TRPACTLMID2>(query, null, transaction: Transaction, commandType: CommandType.Text);
        }

        public int UpdateTRPACTLMID2(TRPACTLMID2 entity)
        {
            var query = "UPDATE dbo.TRPACTLMID2 SET DAT_ATU = '"+ entity.DAT_ATU + "',XML_ANX = '" + entity.XML_ANX + "',XML_ANX_ORI = '" + entity.XML_ANX_ORI + "',MID_ID_ITM_ANX = '" + entity.MID_ID_ITM_ANX + "',MID_NOM_ITM_ANX = '" + entity.MID_NOM_ITM_ANX + "',CAP_NOM_ANX = '" + entity.CAP_NOM_ANX + "',XML_DAT_CMP = '" + entity.XML_DAT_CMP + "', TIP_XML='" + entity.TIP_XML + "', XML_VER='" + entity.XML_VER + "'  WHERE ID_REG=" + entity.ID_REG;

            return Connection.Execute(query, null, transaction: Transaction, commandType: CommandType.Text);
        }

        public int AddTRPACTLMID2(TRPACTLMID2 entity)
        {
            var query = "INSERT INTO dbo.TRPACTLMID2(CAP_ID_NOT,CAP_NUM_CHM,MID_ID_EVO,CAP_NOM_ANX,DAT_CRI,DAT_ATU,XML_ANX,XML_ANX_ORI,TIP_XML,MID_ID_ITM_ANX,MID_NOM_ITM_ANX,XML_VER,XML_DAT_CMP,STU_DOC_FIS,STU_CEPOM,STU_PREF)VALUES('" + entity.CAP_ID_NOT + "','" + entity.CAP_NUM_CHM + "','" + entity.MID_ID_EVO + "','" + entity.CAP_NOM_ANX + "','" + entity.DAT_CRI + "','" + entity.DAT_ATU + "','" + entity.XML_ANX + "','" + entity.XML_ANX_ORI + "','" + entity.TIP_XML + "','" + entity.MID_ID_ITM_ANX + "','" + entity.MID_NOM_ITM_ANX + "','" + entity.XML_VER + "','" + entity.XML_DAT_CMP + "','" + entity.STU_DOC_FIS + "','" + entity.STU_CEPOM + "','" + entity.STU_PREF + "')";

            return Connection.Execute(query, null, transaction: Transaction, commandType: CommandType.Text);
        }
    }
}
