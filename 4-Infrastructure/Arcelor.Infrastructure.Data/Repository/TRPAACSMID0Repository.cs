﻿using Dapper;
using Robo.Domain.Aggregates.ClienteAgg;
using Robo.Domain.Aggregates.RepositoryContract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Robo.Infrastructure.Data.Repository
{
    internal class TRPAACSMID0Repository : RepositoryBase, ITRPAACSMID0Repository
    {
        public TRPAACSMID0Repository(IDbTransaction transaction)
            : base(transaction)
        {            
        }

        public int AddTRPAACSMID0(TRPAACSMID0 entity)
        {
            var query = "INSERT INTO dbo.TRPAACSMID0(TS_ACS, CD_LGN, NM_SER) VALUES ('"+ entity.TS_ACS + "', '" + entity.CD_LGN + "', '" + entity.NM_SER + "')";
          
            return Connection.Execute(query, null, transaction: Transaction, commandType: CommandType.Text);            
        }       
    }
}
