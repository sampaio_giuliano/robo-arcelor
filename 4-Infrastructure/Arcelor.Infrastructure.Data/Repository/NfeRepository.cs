﻿using Dapper;
using Robo.Domain.Aggregates.MidasAgg;
using Robo.Domain.Aggregates.RepositoryContract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robo.Infrastructure.Data.Repository
{
    internal class NfeRepository : RepositoryBase, INfeRepository
    {
        public NfeRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public Nfe GetLastNFe()
        {
            var param = new DynamicParameters();
            var query = "SP_GET_LAST_NFE";
            var item  = Connection.Query<Nfe>(query, null, transaction: Transaction, commandType: CommandType.StoredProcedure);           

            return item.First();
        }

        public bool SetXML(string xml)
        {
            var param = new DynamicParameters();
            param.Add("@xml_txt", xml);

            var query = "SP_SET_NFE_XML";
            int response = Connection.Execute(query, param, transaction: Transaction, commandType: CommandType.StoredProcedure);

            return (response == -1) ? true : false;
        }
    }
}