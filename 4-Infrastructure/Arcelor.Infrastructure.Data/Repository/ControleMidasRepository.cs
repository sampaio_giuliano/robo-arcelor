﻿using Dapper;
using Robo.Domain.Aggregates.ClienteAgg;
using Robo.Domain.Aggregates.MidasAgg;
using Robo.Domain.Aggregates.RepositoryContract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Robo.Infrastructure.Data.Repository
{
    internal class ControleMidasRepository : RepositoryBase, IControleMidasRepository
    {
        public ControleMidasRepository(IDbTransaction transaction)
            : base(transaction)
        {            
        }

        public IEnumerable<ControleMidas> GetControleMidas()
        {
            var query = "SP_RPA_CONSULTAR_CONTROLE_MIDAS";            
            
            var list = Connection.Query<ControleMidas>(query, null,transaction: Transaction, commandType: CommandType.StoredProcedure);
            return list;
        }
        
        public int GetCRPAGETXML0(ControleMidas entity)
        {
            var query = "CRPAGETXML0";
            int retorno = 0;

            var param = new DynamicParameters();
            param.Add("@rec_cap_num_chamado", entity.CHM_CAP);
            param.Add("@rec_cap_nom_anx", entity.STU_XML);
            param.Add("@rec_mid_num_chamado", entity.ID_EVO);
            param.Add("@result_return", retorno);

            var resultado = Connection.Query(query, param, transaction: Transaction, commandType: CommandType.StoredProcedure);

            return retorno;
        }

        public int UpdateTRPACTLMID0(ControleMidas entity)
        {
            var query = "UPDATE dbo.TRPACTLMID0 SET STU_RB_FIS = '" + entity.STU_RB_FIS + "',DAT_ATU = '" + entity.DAT_ATU + "' WHERE ID_REG=" + entity.ID_REG;

            return Connection.Execute(query, null, transaction: Transaction, commandType: CommandType.Text);
        }
    }
}
