﻿using Dapper;
using Robo.Domain.Aggregates.ClienteAgg;
using Robo.Domain.Aggregates.RepositoryContract;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Robo.Infrastructure.Data.Repository
{
    internal class AuditoriaRepository : RepositoryBase, IAuditoriaRepository
    {
        public AuditoriaRepository(IDbTransaction transaction)
            : base(transaction)
        {            
        }

        public int AddAuditoria(Auditoria entity)
        {
            var query = "INSERT INTO dbo.Auditoria (IdExecucao, TipoAgente, DataHoraIni, DataHoraFim, DuracaoProcesso, CategoriaProcesso, CodigoProcesso, ItemProcesso, SubitemProcesso, NomeProcesso, StatusProcesso, MotivoProcesso, NomeEquipamento, IpEquipamento, VersaoRobo) VALUES('" + entity.IdExecucao + "','" + entity.TipoAgente + "','" + entity.DataHoraIni + "','" + entity.DataHoraFim + "'," + entity.DuracaoProcesso + ",'" + entity.CategoriaProcesso + "','" + entity.CodigoProcesso + "','" + entity.ItemProcesso + "','" + entity.SubitemProcesso + "','" + entity.NomeProcesso + "','" + entity.StatusProcesso + "','" + entity.MotivoProcesso + "','" + entity.NomeEquipamento + "','" + entity.IpEquipamento + "','" + entity.VersaoRobo + "')";

            return Connection.Execute(query, null, transaction: Transaction, commandType: CommandType.Text);
        }
    }
}
