﻿using Robo.Domain.Aggregates.RepositoryContract;
using Robo.Domain.Aggregates.UnitOfWork;
using Robo.Infrastructure.Data.Repository;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Robo.Infrastructure.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private IControleMidasRepository _creditoCertoClienteRepository;
        private ITRPAACSMID0Repository _tRPAACSMID0Repository;
        private ITRPACTLMID2Repository _tRPACTLMID2Repository;
        private IAuditoriaRepository _auditoriaRepository;
        private INfeRepository _nfeRepository;

        private bool _disposed;

        public UnitOfWork(string connectionString)
        {
            _connection = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ConnectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public IControleMidasRepository ControleMidasRepository
        {
            get { return _creditoCertoClienteRepository ?? (_creditoCertoClienteRepository = new ControleMidasRepository(_transaction)); }
        }

        public ITRPAACSMID0Repository TRPAACSMID0Repository
        {
            get { return _tRPAACSMID0Repository ?? (_tRPAACSMID0Repository = new TRPAACSMID0Repository(_transaction)); }
        }

        public ITRPACTLMID2Repository TRPACTLMID2Repository
        {
            get { return _tRPACTLMID2Repository ?? (_tRPACTLMID2Repository = new TRPACTLMID2Repository(_transaction)); }
        }

        public IAuditoriaRepository AuditoriaRepository
        {
            get { return _auditoriaRepository ?? (_auditoriaRepository = new AuditoriaRepository(_transaction)); }
        }

        public INfeRepository NfeRepository
        {
            get { return _nfeRepository ?? (_nfeRepository = new NfeRepository(_transaction)); }
        }


        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                resetRepositories();
            }
        }

        private void resetRepositories()
        {
            _creditoCertoClienteRepository = null;
            _tRPAACSMID0Repository = null;
            _tRPACTLMID2Repository = null;
            _auditoriaRepository = null;
            _nfeRepository = null;
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~UnitOfWork()
        {
            dispose(false);
        }
    }
}
