﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Robo.Infrastructure.RoboBase
{
    public static class RobotUtils
    {
        public static string ConverterDataParaString(DateTime? data, string formato = "dd/MM/yyyy HH:mm:ss ffff")
        {
            string valor = string.Format("{0: " + formato + "}", data);
            return valor;
        }
        public static int? ConverterParaIntNullable(string valor)
        {
            int? retorno = null;
            int valorConvertido;

            if (int.TryParse(valor, out valorConvertido)) { retorno = valorConvertido; }

            return retorno;
        }
        public static DateTime? ConverterParaDateTimeNullable(string valor)
        {
            DateTime? retorno = null;
            DateTime valorConvertido;

            if (DateTime.TryParse(valor, out valorConvertido)) { retorno = valorConvertido; }

            return retorno;
        }
        public static string LimparString(this string valor)
        {
            if (!string.IsNullOrEmpty(valor))
            {
                valor = valor.Replace("\t", string.Empty).Replace("\r", string.Empty).Replace("\n", string.Empty).Replace("&nbsp;", "").Trim();
            }
            
            return valor;
        }
        public static long ConvertToTimestamp(DateTime value)
        {
            DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalMilliseconds;
        }

        public static string LimparString(this string valor, params string[] outros)
        {
            if (!string.IsNullOrEmpty(valor))
            {
                valor = valor.Replace("\t", string.Empty).Replace("\r", string.Empty).Replace("\n", string.Empty).Replace("&nbsp;", "").Trim();
            }

            foreach (string outro in outros)
            {
                valor = valor.Replace(outro, string.Empty);
            }

            return valor;
        }

    }
}
