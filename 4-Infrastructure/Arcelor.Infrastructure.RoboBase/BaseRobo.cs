﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Robo.Infrastructure.RoboBase
{
    public abstract class BaseRobo
    {
        public BaseRobo()
        {            
        }        
        
        #region [ Membros Privados ]

        private RestClient _restClient;
        private RestRequest _restRequest;
        
        #endregion

        #region [ Propriedades ]       

        public RestClient RestClient
        {
            get
            {
                if (_restClient == null)
                    _restClient = new RestClient();

                return _restClient;
            }
            set { _restClient = value; }
        }

        public RestRequest RestRequest
        {
            get
            {
                if (_restRequest == null)
                    _restRequest = new RestRequest();

                return _restRequest;
            }
            set { _restRequest = value; }
        }

        #endregion

        #region Métodos Publicos

        public JObject Get(string url)
        {
            lock (this)
            {
                RestClient.BaseUrl = new Uri(url);
                
                RestRequest.Method = Method.GET;

                IRestResponse response = RestClient.Execute(RestRequest);

                return JsonConvert.DeserializeObject<JObject>(response.Content);
            }
        }

        public JObject PostJson(string url, List<Parameter> parameters)
        {
            lock (this)
            {
                RestClient.BaseUrl = new Uri(url);

                foreach (var parameter in parameters)
                    RestRequest.AddParameter(parameter);

                RestRequest.Method = Method.POST;

                IRestResponse response = RestClient.Execute(RestRequest);

                return JsonConvert.DeserializeObject<JObject>(response.Content.Replace("@", ""));
            }
        }

        public JObject PostJson(string url, Parameter parameter)
        {
            try
            {
                lock (this)
                {

                    RestClient.BaseUrl = new Uri(url);

                    RestRequest.AddParameter(parameter);

                    RestRequest.Method = Method.POST;

                    IRestResponse response = RestClient.Execute(RestRequest);

                    return JsonConvert.DeserializeObject<JObject>(response.Content.Replace("@", ""));

                }
            }
            catch(Exception ex)
            { }
            finally
            {
                RestClient = null;
            }
            return null;
        }
        
        #endregion

        public BaseRobo CacheControl(string dados)
        {
            this.RestRequest.AddHeader("Cache-Control",dados);
            return this;
        }

        public BaseRobo Accept(string dados)
        {
            this.RestRequest.AddHeader("Accept", dados);
            return this;
        }

        public BaseRobo Authorization(string dados)
        {
            this.RestRequest.AddHeader("Authorization", dados);
            return this;
        }

        public BaseRobo AcceptLanguage(string dados)
        {
            this.RestRequest.AddHeader("Accept-Language", dados);
            return this;
        }

        public BaseRobo UserAgent(string dados)
        {
            this.RestRequest.AddHeader("User-Agent", dados);
            return this;
        }

        public BaseRobo KbnVersion(string dados)
        {
            this.RestRequest.AddHeader("kbn-version", dados);
            return this;
        }
        public BaseRobo ContentType(string dados)
        {
            this.RestRequest.AddHeader("Content-Type", dados);
            return this;
        }

    }
}