﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Robo.Infrastructure.CrossCutting.SeedWork.Threads
{
    public class ThreadService
    {
        #region [Membros Privados]

        private System.Threading.Timer _Timer = null;
        private DateTime? _proximaExecucao { get; set; }
        private DateTime? _ultimaExecucao { get; set; }
        private bool _execucaoParada { get; set; }
        private bool _estaExecutando { get; set; }

        #endregion

        #region [Membros Publicos]

        public bool EstaEmHorarioDeExecucao { get { return VerificarDia(DateTime.Now) && VerificarHorario(DateTime.Now.TimeOfDay); } }
        public DateTime? ProximaExecucao { get { return _proximaExecucao; } }
        public DateTime? UltimaExecucao { get { return _ultimaExecucao; } }

        #endregion

        #region [Fluent]

        private bool _TodosOsDias { get; set; }
        public ThreadService TodosOsDias(bool dados, bool calcularProximaExecucao)
        {
            this._TodosOsDias = dados;
            CalcularProximaExecucao(calcularProximaExecucao);
            return this;
        }

        private bool _NaSegunda { get; set; }
        public ThreadService NaSegunda(bool dados, bool calcularProximaExecucao)
        {
            this._NaSegunda = dados;
            CalcularProximaExecucao(calcularProximaExecucao);
            return this;
        }

        private bool _NaTerca { get; set; }
        public ThreadService NaTerca(bool dados, bool calcularProximaExecucao)
        {
            this._NaTerca = dados;
            CalcularProximaExecucao(calcularProximaExecucao);
            return this;
        }

        private bool _NaQuarta { get; set; }
        public ThreadService NaQuarta(bool dados, bool calcularProximaExecucao)
        {
            this._NaQuarta = dados;
            CalcularProximaExecucao(calcularProximaExecucao);
            return this;
        }

        private bool _NaQuinta { get; set; }
        public ThreadService NaQuinta(bool dados, bool calcularProximaExecucao)
        {
            this._NaQuinta = dados;
            CalcularProximaExecucao(calcularProximaExecucao);
            return this;
        }

        private bool _NaSexta { get; set; }
        public ThreadService NaSexta(bool dados, bool calcularProximaExecucao)
        {
            this._NaSexta = dados;
            CalcularProximaExecucao(calcularProximaExecucao);
            return this;
        }

        private bool _NoSabado { get; set; }
        public ThreadService NoSabado(bool dados, bool calcularProximaExecucao)
        {
            this._NoSabado = dados;
            CalcularProximaExecucao(calcularProximaExecucao);
            return this;
        }

        private bool _NoDomingo { get; set; }
        public ThreadService NoDomingo(bool dados, bool calcularProximaExecucao)
        {
            this._NoDomingo = dados;
            CalcularProximaExecucao(calcularProximaExecucao);
            return this;
        }

        private TimeSpan _InicioProcessamento { get; set; }
        public ThreadService InicioProcessamento(TimeSpan dados, bool calcularProximaExecucao)
        {
            this._InicioProcessamento = dados;
            CalcularProximaExecucao(calcularProximaExecucao);
            return this;
        }

        private TimeSpan _IntervaloProcessamento { get; set; }
        public ThreadService IntervaloProcessamento(TimeSpan dados, bool calcularProximaExecucao)
        {
            this._IntervaloProcessamento = dados;
            CalcularProximaExecucao(calcularProximaExecucao);
            return this;
        }

        private TimeSpan _FimProcessamento { get; set; }
        public ThreadService FimProcessamento(TimeSpan dados, bool calcularProximaExecucao)
        {
            this._FimProcessamento = dados;
            CalcularProximaExecucao(calcularProximaExecucao);
            return this;
        }

        private bool _IniciarProcessoImediatamente { get; set; }
        public ThreadService IniciarProcessoImediatamente(bool dados, bool calcularProximaExecucao)
        {
            this._IniciarProcessoImediatamente = dados;
            CalcularProximaExecucao(calcularProximaExecucao);
            return this;
        }

        private Action _OnExecutarProcesso { get; set; }
        public ThreadService OnExecutarProcesso(Action dados)
        {
            this._OnExecutarProcesso = dados;
            return this;
        }

        private Action _OnProcessoFinalizado { get; set; }
        public ThreadService OnProcessoFinalizado(Action dados)
        {
            this._OnProcessoFinalizado = dados;
            return this;
        }

        public ThreadService Iniciar()
        {
            if (_OnExecutarProcesso != null)
            {
                _execucaoParada = false;
                TimeSpan tempoParaInicio = _IniciarProcessoImediatamente ? new TimeSpan(0, 0, 0) : CalcularProximaExecucao();
                _Timer = new System.Threading.Timer(new System.Threading.TimerCallback(ExecutarProcedimento), null, tempoParaInicio, _IntervaloProcessamento);
            }

            return this;
        }
        public ThreadService Parar()
        {
            if (_OnExecutarProcesso != null)
            {
                _Timer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
                _execucaoParada = true;
                _proximaExecucao = null;
            }

            return this;
        }

        #endregion

        private void ExecutarProcedimento(object o)
        {
            if (!_estaExecutando)
            {
                try
                {
                    if (_OnExecutarProcesso != null && EstaEmHorarioDeExecucao)
                    {
                        _estaExecutando = true;
                        _Timer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
                        _OnExecutarProcesso.Invoke();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _estaExecutando = false;
                    _ultimaExecucao = DateTime.Now;

                    if (!_execucaoParada)
                    {
                        _Timer.Change(CalcularProximaExecucao(), _IntervaloProcessamento);
                    }

                    if (this._OnProcessoFinalizado != null)
                    {
                        this._OnProcessoFinalizado.Invoke();
                    }
                }
            }
        }
        private bool VerificarDia(DateTime dataParaVerificacao)
        {
            bool retorno;

            switch (dataParaVerificacao.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    retorno = _NoDomingo;
                    break;
                case DayOfWeek.Monday:
                    retorno = _NaSegunda;
                    break;
                case DayOfWeek.Tuesday:
                    retorno = _NaTerca;
                    break;
                case DayOfWeek.Wednesday:
                    retorno = _NaQuarta;
                    break;
                case DayOfWeek.Thursday:
                    retorno = _NaQuinta;
                    break;
                case DayOfWeek.Friday:
                    retorno = _NaSexta;
                    break;
                case DayOfWeek.Saturday:
                    retorno = _NoSabado;
                    break;
                default:
                    retorno = false;
                    break;
            }

            return retorno || _TodosOsDias;
        }
        private bool VerificarHorario(TimeSpan horarioParaVerificao)
        {
            bool aposPeriodoInicio = _InicioProcessamento == null || _InicioProcessamento <= horarioParaVerificao;
            bool antesPeriodoFim = _FimProcessamento == null || _FimProcessamento >= horarioParaVerificao;
            return aposPeriodoInicio && antesPeriodoFim;
        }
        private void CalcularProximaExecucao(bool fazerCalculo)
        {
            if (fazerCalculo)
            {
                CalcularProximaExecucao();
            }
        }
        private TimeSpan CalcularProximaExecucao()
        {
            try
            {
                _proximaExecucao = DateTime.Now.Date.Add(_InicioProcessamento);
                DateTime dataParaCalculo = DateTime.Now.AddSeconds(3);
                TimeSpan intervalo = _IntervaloProcessamento == null ||
                                     _IntervaloProcessamento == new TimeSpan() ||
                                     _IntervaloProcessamento == new TimeSpan(0, 0, 0) ?
                                     new TimeSpan(0, 5, 0) :
                                     _IntervaloProcessamento;

                #region [Determinar horario da próxima execução]

                while (!(_proximaExecucao > dataParaCalculo))
                {
                    _proximaExecucao = _proximaExecucao.Value.Add(intervalo);
                }

                if (_proximaExecucao.Value.TimeOfDay >= _FimProcessamento)
                {
                    _proximaExecucao = _proximaExecucao.Value.Date.AddDays(1).Add(_InicioProcessamento);
                }

                if (_proximaExecucao.Value.TimeOfDay < _InicioProcessamento)
                {
                    _proximaExecucao = _proximaExecucao.Value.Date.Add(intervalo);
                }

                #endregion

                #region [Determina o dia da próxima execução]

                while (!VerificarDia(_proximaExecucao.Value))
                {
                    _proximaExecucao = _proximaExecucao.Value.AddDays(1);
                }

                #endregion

                return _proximaExecucao.Value.Add(-DateTime.Now.TimeOfDay).TimeOfDay;
            }
            catch
            {
                return new TimeSpan();
            }
        }
    }
}
