﻿using Robo.Infrastructure.Data.UnitOfWork;
using Robo.Infrastructure.Web.Midas;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Xml.Linq;

namespace RoboApp.Application.Service.Servico
{
    public class RoboAppService : IDisposable
    {
        public RoboAppService() { }

        public Action<string> OnEnvioDeInformacao;

        private void RaiseEnvioDeInformacao(string retorno)
        {
            if (OnEnvioDeInformacao != null) { OnEnvioDeInformacao(retorno); }
        }

        public static string SeparadorDeLinha()
        {
            return "-//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//-" + Environment.NewLine;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        public void ProcessarServico(TimeSpan? inicioProcessamento, TimeSpan? fimProcessamento, ref bool executando, int quantidade = 0, string textoSMS = null, TimeSpan? intervaloProcessamento = null)
        {
            try
            {

                var currentTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

                using (var uow = new UnitOfWork("ConnectionString"))
                {
                    uow.AuditoriaRepository.AddAuditoria(new Robo.Domain.Aggregates.ClienteAgg.Auditoria()
                    {
                        IdExecucao = "SPF15" + DateTime.Now.ToString("ddMMyyyyHHmmss"),
                        TipoAgente = "Robô",
                        DataHoraIni = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        DataHoraFim = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        DuracaoProcesso = 0,
                        CategoriaProcesso = "Recebimento Fiscal",
                        CodigoProcesso = "SUPFISC15",
                        ItemProcesso = "Acionamento",
                        SubitemProcesso = "Acionamento",
                        NomeProcesso = "Suporte Fiscal 1.5",
                        StatusProcesso = "Sucesso",
                        MotivoProcesso = "-",
                        NomeEquipamento = Environment.MachineName,
                        IpEquipamento = GetLocalIPAddress(),
                        VersaoRobo = "V1.0"
                    });

                    
                    uow.Commit();

                    ServicoMidas servicoMidas = new ServicoMidas();
                    var retornoAuthenticate = servicoMidas.AuthenticateRequest(ConfigurationManager.AppSettings["UsuarioMidas"], ConfigurationManager.AppSettings["SenhaMidas"]);

                    if (retornoAuthenticate.Body.AuthenticateRequestResponse.AuthenticateRequestResult.Length != 36)
                    {
                        throw new Exception("MIDAS Erro: Problemas no acesso ao serviço AuthenticateRequest SessionId inválido(Length)");
                    }

                    else if (retornoAuthenticate.Body.AuthenticateRequestResponse.AuthenticateRequestResult.ToString().Equals("0000000-0000-0000-0000000000000000") || string.IsNullOrEmpty(retornoAuthenticate.Body.AuthenticateRequestResponse.AuthenticateRequestResult.ToString()))
                    {                        
                        throw new Exception("MIDAS Erro: Problemas no acesso ao serviço AuthenticateRequest SessionId inválido(Conteúdo)");
                    }
                                        
                    uow.TRPAACSMID0Repository.AddTRPAACSMID0(new Robo.Domain.Aggregates.ClienteAgg.TRPAACSMID0()
                    {
                        CD_LGN = ConfigurationManager.AppSettings["UsuarioMidas"],
                        TS_ACS = currentTime.ToString(),
                        NM_SER = "GetStatusPreOccurrence"
                    });
                                        
                    uow.Commit();

                    var retornoConsultaControleMidas = uow.ControleMidasRepository.GetControleMidas();

                    uow.Commit();

                    string preOccurrenceId = string.Empty;

                    foreach (var controleMidas in retornoConsultaControleMidas)
                    {
                        preOccurrenceId += controleMidas.ID_EVO + ",";
                    }

                    var retornoStatus = servicoMidas.GetStatusPreOccurrence(retornoAuthenticate.Body.AuthenticateRequestResponse.AuthenticateRequestResult, preOccurrenceId);

                    
                    if (retornoStatus.Body.GetStatusPreOccurrenceResponse.GetStatusPreOccurrenceResult.Length > 0)
                    {
                        foreach (var itemGetStatusPreOccurrence in retornoStatus.Body.GetStatusPreOccurrenceResponse.GetStatusPreOccurrenceResult)
                        {
                            try
                            {
                                if (itemGetStatusPreOccurrence.HasError == false)
                                {                                    
                                    var todosFinished = itemGetStatusPreOccurrence.Items.Where(p => p.Status.Trim().Equals("Returned") || p.Status.Trim().Equals("Denied"));

                                    if ((itemGetStatusPreOccurrence.Status.ToUpper().Equals("FINISHED") || itemGetStatusPreOccurrence.Status.ToUpper().Equals("DENIED") || itemGetStatusPreOccurrence.Status.ToUpper().Equals("INVALID") || itemGetStatusPreOccurrence.Status.ToUpper().Equals("REJECT") || itemGetStatusPreOccurrence.Status.ToUpper().Equals("ERROR")) &&
                                        todosFinished.Count() == itemGetStatusPreOccurrence.Items.Count())
                                    {
                                        var chamadoCAP = retornoConsultaControleMidas.FirstOrDefault(p => p.ID_EVO.Trim().Equals(itemGetStatusPreOccurrence.PreOccurrenceId.ToString()));

                                        uow.ControleMidasRepository.UpdateTRPACTLMID0(new Robo.Domain.Aggregates.ClienteAgg.ControleMidas()
                                        {
                                            ID_REG = chamadoCAP.ID_REG,
                                            STU_RB_FIS = "DENIED",
                                            DAT_ATU = currentTime
                                        });

                                        uow.Commit();

                                        continue;
                                    }
                                    else if (itemGetStatusPreOccurrence.Status.ToUpper().Equals("FINISHED"))
                                    {
                                        foreach (var item in itemGetStatusPreOccurrence.Items.Where(p => p.Status.Trim().ToUpper().Equals("FINISHED")))
                                        {
                                            try
                                            {
                                                RaiseEnvioDeInformacao(string.Format("O status retorno foi {1} - Horário: {0}", DateTime.Now, item.Status));
                                                                                                
                                                string statusOcorrencia = string.Empty;

                                                statusOcorrencia = "FINALIZADO";

                                                var chamadoCAP = retornoConsultaControleMidas.FirstOrDefault(p => p.ID_EVO.Trim().Equals(itemGetStatusPreOccurrence.PreOccurrenceId.ToString()));

                                                var retornoTRPACTLMID2 = uow.TRPACTLMID2Repository.GetTRPACTLMID2(new Robo.Domain.Aggregates.ClienteAgg.TRPACTLMID2()
                                                {
                                                    MID_ID_EVO = itemGetStatusPreOccurrence.PreOccurrenceId.ToString(),
                                                    CAP_NUM_CHM = retornoConsultaControleMidas.FirstOrDefault(p => p.ID_EVO.Trim().Equals(itemGetStatusPreOccurrence.PreOccurrenceId.ToString())).CHM_CAP,
                                                    CAP_NOM_ANX = item.FileName.Split('_')[0].Trim() + ".pdf"
                                                });

                                                if (retornoTRPACTLMID2 == null || retornoTRPACTLMID2.Count() == 0)
                                                {
                                                    retornoTRPACTLMID2 = uow.TRPACTLMID2Repository.GetTRPACTLMID2(new Robo.Domain.Aggregates.ClienteAgg.TRPACTLMID2()
                                                    {
                                                        MID_ID_EVO = itemGetStatusPreOccurrence.PreOccurrenceId.ToString(),
                                                        CAP_NUM_CHM = retornoConsultaControleMidas.FirstOrDefault(p => p.ID_EVO.Trim().Equals(itemGetStatusPreOccurrence.PreOccurrenceId.ToString())).CHM_CAP,
                                                        CAP_NOM_ANX = item.FileName.Trim()
                                                    });
                                                }

                                                if (retornoTRPACTLMID2 == null || retornoTRPACTLMID2.Count() == 0)
                                                {
                                                    retornoTRPACTLMID2 = uow.TRPACTLMID2Repository.GetTRPACTLMID2(new Robo.Domain.Aggregates.ClienteAgg.TRPACTLMID2()
                                                    {
                                                        MID_ID_EVO = itemGetStatusPreOccurrence.PreOccurrenceId.ToString(),
                                                        CAP_NUM_CHM = retornoConsultaControleMidas.FirstOrDefault(p => p.ID_EVO.Trim().Equals(itemGetStatusPreOccurrence.PreOccurrenceId.ToString())).CHM_CAP,
                                                        CAP_NOM_ANX = string.Empty
                                                    });
                                                }

                                                RaiseEnvioDeInformacao(string.Format("Tratando o chamado do CAP {1} e midas {2} - Horário: {0}", DateTime.Now, retornoConsultaControleMidas.FirstOrDefault(p => p.ID_EVO.Trim().Equals(itemGetStatusPreOccurrence.PreOccurrenceId.ToString())).CHM_CAP, itemGetStatusPreOccurrence.PreOccurrenceId.ToString()));

                                                uow.AuditoriaRepository.AddAuditoria(new Robo.Domain.Aggregates.ClienteAgg.Auditoria()
                                                {
                                                    IdExecucao = "SPF15" + DateTime.Now.ToString("ddMMyyyyHHmmss"),
                                                    TipoAgente = "Robô",
                                                    DataHoraIni = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                                    DataHoraFim = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                                    DuracaoProcesso = 0,
                                                    CategoriaProcesso = "Recebimento Fiscal",
                                                    CodigoProcesso = "SUPFISC15",
                                                    ItemProcesso = retornoConsultaControleMidas.FirstOrDefault(p => p.ID_EVO.Trim().Equals(itemGetStatusPreOccurrence.PreOccurrenceId.ToString())).CHM_CAP,
                                                    SubitemProcesso = itemGetStatusPreOccurrence.PreOccurrenceId.ToString(),
                                                    NomeProcesso = "Suporte Fiscal 1.5",
                                                    StatusProcesso = "Sucesso",
                                                    MotivoProcesso = "-",
                                                    NomeEquipamento = Environment.MachineName,
                                                    IpEquipamento = GetLocalIPAddress(),
                                                    VersaoRobo = "V1.0"
                                                });

                                                uow.Commit();
                                                                                               
                                                string str_MID_NOM_ITM_ANX = string.Empty;

                                                if (itemGetStatusPreOccurrence.ItensCount > 1)
                                                    str_MID_NOM_ITM_ANX = item.FileName.Replace(".pdf", "").Split('_')[0] + ".pdf";
                                                else
                                                    str_MID_NOM_ITM_ANX = item.FileName;

                                                string xmlMidas = HttpUtility.HtmlDecode(((System.Xml.XmlCharacterData)((System.Xml.XmlNode[])item.IntegrationDocument)[0]).Data.Replace("&", "E"));

                                                var root = XDocument.Parse(xmlMidas, LoadOptions.PreserveWhitespace);
                                                var masterDocument = XDocument.Parse(HttpUtility.HtmlDecode(((System.Xml.XmlNode[])item.MasterDataDocument)[0].Value), LoadOptions.PreserveWhitespace);

                                                string tipoNota = string.Empty;
                                                string versaoNota = string.Empty;

                                                var nfe = root.Root.Element("{http://www.portalfiscal.inf.br/nfe}NFe");

                                                if (nfe != null && !nfe.IsEmpty &&
                                                    ((System.Xml.Linq.XElement)((System.Xml.Linq.XContainer)((System.Xml.Linq.XContainer)nfe.LastNode).FirstNode).LastNode).LastAttribute.Value.ToUpper().Contains("#NFE"))
                                                {
                                                    tipoNota = ((System.Xml.Linq.XElement)((System.Xml.Linq.XContainer)((System.Xml.Linq.XContainer)nfe.LastNode).FirstNode).LastNode).LastAttribute.Value.Substring(1, 3).ToUpper();
                                                }
                                                else if (nfe != null && !nfe.IsEmpty &&
                                                    ((System.Xml.Linq.XElement)((System.Xml.Linq.XContainer)((System.Xml.Linq.XContainer)nfe.LastNode).FirstNode).LastNode).LastAttribute.Value.Substring(26, 2).Equals("57"))
                                                {
                                                    tipoNota = "CTE";
                                                }
                                                else if (nfe != null && !nfe.IsEmpty &&
                                                    ((System.Xml.Linq.XElement)((System.Xml.Linq.XContainer)((System.Xml.Linq.XContainer)nfe.LastNode).FirstNode).LastNode).LastAttribute.Value.Substring(26, 2).Equals("67"))
                                                {
                                                    tipoNota = "CTEOS";
                                                }

                                                var cte = root;

                                                if (cte != null && root.Root.Name.LocalName.Trim().Equals("cteProc"))
                                                {
                                                    tipoNota = "CTE";
                                                }

                                                var particularidade = root.Root.Element("Particularidades");

                                                if (particularidade != null && particularidade.Value.ToUpper().Contains("NFSE"))
                                                {
                                                    tipoNota = particularidade.Value.Substring(0, 4);
                                                }

                                                var subTipo = root.Root.Element("DetalhesDocumento");

                                                if (subTipo != null)
                                                {
                                                    var subTipoTratamento = subTipo.Element("SubTipo").Value;

                                                    switch (subTipoTratamento.ToUpper())
                                                    {
                                                        case "BOLETO":
                                                            tipoNota = subTipoTratamento.ToUpper();
                                                            break;

                                                        case "DIVERSOS":
                                                            tipoNota = subTipoTratamento.Replace("DIVERSOS", "RECIBO");
                                                            break;

                                                        case "RECPJ":
                                                            tipoNota = subTipoTratamento.Replace("RECPJ", "RECIBO");
                                                            break;

                                                        case "RPA":
                                                            tipoNota = subTipoTratamento;
                                                            break;

                                                        case "NFCEE":
                                                        case "NFCFA":
                                                        case "NFCFG":
                                                        case "NFSC":
                                                        case "NFSTE":
                                                            tipoNota = subTipoTratamento;
                                                            break;
                                                    }
                                                }

                                                if (root.Root.Attribute("versao") != null)
                                                    versaoNota = root.Root.Attribute("versao").Value;
                                                else if (cte != null && root.Root.Name.LocalName.Trim().Equals("cteProc"))
                                                    versaoNota = ((System.Xml.Linq.XElement)((System.Xml.Linq.XContainer)root.Root.FirstNode).FirstNode).LastAttribute.Value;
                                                else
                                                    versaoNota = root.Root.FirstAttribute.Value;

                                                var MID2Vazio = retornoTRPACTLMID2.FirstOrDefault(p => p.CAP_NOM_ANX.Trim().Equals(item.FileName.Trim()) || p.CAP_NOM_ANX.Trim().Equals(item.FileName.Split('_')[0].Trim()+".pdf"));
                                                
                                                var ultimoCaracter = root.ToString().IndexOf(">");
                                                var ultimaPosicao = root.ToString().IndexOf(" ");
                                                var valorRemove = root.ToString().Substring(0, ultimoCaracter);
                                                var valorReplace = root.ToString().Substring(0, ultimaPosicao).Trim();

                                                var xmlResultado = root.ToString().Replace(valorRemove, valorReplace);

                                                if (MID2Vazio == null)
                                                {
                                                    uow.TRPACTLMID2Repository.AddTRPACTLMID2(new Robo.Domain.Aggregates.ClienteAgg.TRPACTLMID2()
                                                    {
                                                        CAP_ID_NOT = retornoTRPACTLMID2.FirstOrDefault().CAP_ID_NOT,
                                                        CAP_NUM_CHM = retornoTRPACTLMID2.FirstOrDefault().CAP_NUM_CHM,
                                                        MID_ID_EVO = retornoTRPACTLMID2.FirstOrDefault().MID_ID_EVO,
                                                        DAT_CRI = currentTime,
                                                        DAT_ATU = string.Empty,
                                                        XML_ANX = xmlResultado.Replace("'", ""),
                                                        XML_ANX_ORI = xmlMidas.Replace("'", ""),
                                                        TIP_XML = tipoNota,
                                                        MID_ID_ITM_ANX = item.Id.ToString(),
                                                        MID_NOM_ITM_ANX = str_MID_NOM_ITM_ANX,
                                                        CAP_NOM_ANX = item.FileName,
                                                        XML_VER = versaoNota,
                                                        XML_DAT_CMP = masterDocument.Root.ToString().Replace("'", ""),
                                                        STU_CEPOM = string.Empty,
                                                        STU_DOC_FIS = string.Empty,
                                                        STU_PREF = string.Empty
                                                    });

                                                    uow.Commit();
                                                }
                                                else
                                                {
                                                    uow.TRPACTLMID2Repository.UpdateTRPACTLMID2(new Robo.Domain.Aggregates.ClienteAgg.TRPACTLMID2()
                                                    {
                                                        ID_REG = MID2Vazio.ID_REG,
                                                        DAT_ATU = currentTime,
                                                        XML_ANX = xmlResultado.Replace("'", ""),
                                                        XML_ANX_ORI = xmlMidas.Replace("'", ""),
                                                        MID_ID_ITM_ANX = item.Id.ToString(),
                                                        MID_NOM_ITM_ANX = str_MID_NOM_ITM_ANX,
                                                        CAP_NOM_ANX = item.FileName,
                                                        TIP_XML = tipoNota,
                                                        XML_VER = versaoNota,
                                                        XML_DAT_CMP = masterDocument.Root.ToString().Replace("'", "")
                                                    });

                                                    uow.Commit();
                                                }                                                

                                                uow.ControleMidasRepository.GetCRPAGETXML0(new Robo.Domain.Aggregates.ClienteAgg.ControleMidas()
                                                {
                                                    CHM_CAP = retornoConsultaControleMidas.FirstOrDefault(p => p.ID_EVO.Trim().Equals(itemGetStatusPreOccurrence.PreOccurrenceId.ToString())).CHM_CAP,
                                                    STU_XML = item.FileName,
                                                    ID_EVO = itemGetStatusPreOccurrence.PreOccurrenceId.ToString()
                                                });                                                

                                                //uow.AuditoriaRepository.AddAuditoria(new Robo.Domain.Aggregates.ClienteAgg.Auditoria()
                                                //{
                                                //    IdExecucao = "SPF15" + DateTime.Now.ToString("ddMMyyyyHHmmss"),
                                                //    TipoAgente = "Robô",
                                                //    DataHoraIni = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                                //    DataHoraFim = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                                //    DuracaoProcesso = 0,
                                                //    CategoriaProcesso = "Recebimento Fiscal",
                                                //    CodigoProcesso = "SUPFISC15",
                                                //    ItemProcesso = "Encerramento",
                                                //    SubitemProcesso = "Encerramento",
                                                //    NomeProcesso = "Suporte Fiscal - R1.5",
                                                //    StatusProcesso = "Sucesso",
                                                //    MotivoProcesso = "-",
                                                //    NomeEquipamento = Environment.MachineName,
                                                //    IpEquipamento = GetLocalIPAddress(),
                                                //    VersaoRobo = "V1.0"
                                                //});

                                                uow.Commit();                                                
                                            }
                                            catch (Exception ex)
                                            {
                                                RaiseEnvioDeInformacao(string.Format("Erro ao processar o registro. OcurrenceID:{0} - Erro: {1} {2}", item.OccurrenceId, ex.Message, (ex.InnerException != null ? ex.InnerException.Message : string.Empty)));

                                                uow.AuditoriaRepository.AddAuditoria(new Robo.Domain.Aggregates.ClienteAgg.Auditoria()
                                                {
                                                    IdExecucao = "SPF15" + DateTime.Now.ToString("ddMMyyyyHHmmss"),
                                                    TipoAgente = "Robô",
                                                    DataHoraIni = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                                    DataHoraFim = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                                    DuracaoProcesso = 0,
                                                    CategoriaProcesso = "Recebimento Fiscal",
                                                    CodigoProcesso = "SUPFISC15",
                                                    ItemProcesso = retornoConsultaControleMidas.FirstOrDefault(p => p.ID_EVO.Trim().Equals(itemGetStatusPreOccurrence.PreOccurrenceId.ToString())).CHM_CAP,
                                                    SubitemProcesso = itemGetStatusPreOccurrence.PreOccurrenceId.ToString(),
                                                    NomeProcesso = "Suporte Fiscal 1.5",
                                                    StatusProcesso = "Falha",
                                                    MotivoProcesso = ex.Message,
                                                    NomeEquipamento = Environment.MachineName,
                                                    IpEquipamento = GetLocalIPAddress(),
                                                    VersaoRobo = "V1.0"
                                                });

                                                uow.Commit();
                                            }

                                            RaiseEnvioDeInformacao(string.Format("Finalizando o tratamento do item - Horário: {0}", DateTime.Now));
                                        }
                                    }

                                    if (itemGetStatusPreOccurrence.Status.ToUpper().Equals("FINISHED"))
                                    {
                                        var chamadoCAPPrincipal = retornoConsultaControleMidas.FirstOrDefault(p => p.ID_EVO.Trim().Equals(itemGetStatusPreOccurrence.PreOccurrenceId.ToString()));

                                        uow.ControleMidasRepository.UpdateTRPACTLMID0(new Robo.Domain.Aggregates.ClienteAgg.ControleMidas()
                                        {
                                            ID_REG = chamadoCAPPrincipal.ID_REG,
                                            STU_RB_FIS = "PROCESSADO",
                                            DAT_ATU = currentTime
                                        });

                                        uow.Commit();
                                    }
                                    else
                                    {
                                        var chamadoCAPPrincipal = retornoConsultaControleMidas.FirstOrDefault(p => p.ID_EVO.Trim().Equals(itemGetStatusPreOccurrence.PreOccurrenceId.ToString()));

                                        uow.ControleMidasRepository.UpdateTRPACTLMID0(new Robo.Domain.Aggregates.ClienteAgg.ControleMidas()
                                        {
                                            ID_REG = chamadoCAPPrincipal.ID_REG,
                                            STU_RB_FIS = "EM PROCESSAMENTO",
                                            DAT_ATU = currentTime
                                        });

                                        uow.Commit();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                RaiseEnvioDeInformacao(string.Format("Erro ao processar o registro. PreOcurrenceID:{0} - Erro: {1} {2}", itemGetStatusPreOccurrence.PreOccurrenceId, ex.Message, (ex.InnerException != null ? ex.InnerException.Message : string.Empty)));                               
                            }
                        }
                    }

                    uow.AuditoriaRepository.AddAuditoria(new Robo.Domain.Aggregates.ClienteAgg.Auditoria()
                    {
                        IdExecucao = "SPF15" + DateTime.Now.ToString("ddMMyyyyHHmmss"),
                        TipoAgente = "Robô",
                        DataHoraIni = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        DataHoraFim = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        DuracaoProcesso = 0,
                        CategoriaProcesso = "Recebimento Fiscal",
                        CodigoProcesso = "SUPFISC15",
                        ItemProcesso = "Encerramento",
                        SubitemProcesso = "Encerramento",
                        NomeProcesso = "Suporte Fiscal 1.5",
                        StatusProcesso = "Sucesso",
                        MotivoProcesso = "-",
                        NomeEquipamento = Environment.MachineName,
                        IpEquipamento = GetLocalIPAddress(),
                        VersaoRobo = "V1.0"
                    });

                    uow.Commit();
                }
            }
            catch (Exception ex)
            {
                RaiseEnvioDeInformacao(string.Format("Erro ao processar o registro. {0} {1}", ex.Message, (ex.InnerException != null ? ex.InnerException.Message : string.Empty)));
            }

            RaiseEnvioDeInformacao(string.Format("Finalizando o processamento das notas: {0}", DateTime.Now));

            RaiseEnvioDeInformacao(SeparadorDeLinha());

            executando = false;
        }

        public void Dispose()
        {

        }
    }
}
