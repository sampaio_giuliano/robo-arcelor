﻿using Robo.Domain.Aggregates.MidasAgg;
using Robo.Infrastructure.Data.UnitOfWork;
using Robo.Infrastructure.Web.Midas;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace RoboApp.Application.Service.Servico
{
    public class RoboArcelorService : IDisposable
    {
        public Action<string> OnEnvioDeInformacao;

        public RoboArcelorService()
        {
        }

        public void Dispose()
        {
        }

        private void RaiseEnvioDeInformacao(string retorno)
        {
            if (OnEnvioDeInformacao != null) { OnEnvioDeInformacao(retorno); }
        }

        public static string SeparadorDeLinha()
        {
            return "-//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//-" + Environment.NewLine;
        }

        public void ProcessarServico(TimeSpan? inicioProcessamento, TimeSpan? fimProcessamento, ref bool executando, int quantidade = 0, string textoSMS = null, TimeSpan? intervaloProcessamento = null)
        {
            try
            {
                this.LogIt(0);

                using (var uow = new UnitOfWork("ConnectionString"))
                {
                    int qty = 0;

                    do
                    {
                        var nfe = this.GetLastNfe(uow);
                        var collection = this.GetNfeFromWS(nfe.chave_nfe);

                        qty = collection.Length;

                        this.LogIt(string.Format("Chave da NFe: {0}", nfe.chave_nfe));
                        this.LogIt(string.Format("Quantidade de NFe encontrados: {0}", qty));

                        foreach (string xml in collection)
                        {
                            string xmlContent = handleXml(xml);
                            
                            var response = this.SetNfeXML(uow, xmlContent);

                            if (!response)
                            {
                                this.LogIt("Não foi possível cadastrar o xml");
                            }

                            this.LogIt(4);
                        }
                    } while (qty == 100);

                    RaiseEnvioDeInformacao(SeparadorDeLinha());
                }

            }
            catch (Exception e)
            {
                string log = string.Format(e.Message, DateTime.Now);
                RaiseEnvioDeInformacao(log);
                RaiseEnvioDeInformacao(SeparadorDeLinha());
            }

            executando = false;
        }

        /// <summary>
        /// Retorna o xml com as notas fiscais
        /// </summary>
        protected string[] GetNfeFromWS(string nfeKey)
        {
            this.LogIt(2);

            ServicoNFe service = new ServicoNFe();

            return service.GetNfes(nfeKey);
        }

        /// <summary>
        /// Retorna a chave com a última chave de NFe inserida no BD
        /// </summary>
        /// <param name="uow"></param>
        /// <returns></returns>
        protected Nfe GetLastNfe(UnitOfWork uow)
        {
            this.LogIt(1);

            var nfeKey = uow.NfeRepository.GetLastNFe();
            uow.Commit();

            return nfeKey;
        }

        /// <summary>
        /// Insere um XML na base de dados através de Proc
        /// </summary>
        /// <param name="uow"></param>
        /// <param name="xml"></param>
        protected bool SetNfeXML(UnitOfWork uow, string xml)
        {
            this.LogIt(3);

            var response = uow.NfeRepository.SetXML(xml);
            uow.Commit();

            return response;
        }

        /// <summary>
        /// Executa a mensagem de log, passando indice da mensagem 
        /// </summary>
        /// <param name="index"></param>
        protected void LogIt(int index)
        {
            string[] msg = new string[10];

            msg[0] = "Inciando o processamento: {0}";
            msg[1] = "Buscando o último chave de Nfe: {0}";
            msg[2] = "Retornando a lista de notas fiscais do web service: {0}";
            msg[3] = "Insere nota fiscal: {0}";
            msg[4] = "Nota fiscal inserido com sucesso: {0}";

            var file = @"log.txt";
            string log = string.Format(msg[index], DateTime.Now);

            System.IO.File.WriteAllText(file, log);

            RaiseEnvioDeInformacao(log);
        }

        /// <summary>
        /// Executa a mensagem de log, passando o texto da mensagem  
        /// </summary>
        /// <param name="msg"></param>
        protected void LogIt(string msg)
        {
            msg += ": {0}";

            string log = string.Format(msg, DateTime.Now);

            RaiseEnvioDeInformacao(log);
        }

        /// <summary>
        /// Manipula a string do xml para remover cacteres indesejados
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        protected string handleXml(string xml)
        {
            xml = xml.Replace("[<![CDATA[]", "")
                     .Replace("[]]>]", "");
            
            return xml;
        }
    }
}
