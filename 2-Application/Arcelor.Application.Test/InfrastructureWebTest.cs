using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Robo.Application.Test
{
    [TestClass]
    public class InfrastructureWebTest
    {
        [TestMethod]
        public void TesteRoboAutenticacao()
        {
            var robo = new Robo.Infrastructure.Web.Midas.ServicoMidas();

            var retorno = robo.AuthenticateRequest("WCFGLOBO", "T1W7CO2M");

            string sessionId = retorno.Body.AuthenticateRequestResponse.AuthenticateRequestResult;

            var retornoStatus = robo.GetStatusPreOccurrence(sessionId, "4050628,4050629");
        }        
    }
}
